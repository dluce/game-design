package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.danielluce.mappening.GameCore;

import scenes.HUD;
import sprites.Hero;
import tools.TileMapObjectParser;

public class GameScreen implements Screen
{
   private final GameCore gameCore;
   private OrthographicCamera gameCam;
   private Viewport viewPort;
   private HUD hud;

   // Define map variables
   private TmxMapLoader mapLoader;
   private TiledMap map;
   private OrthogonalTiledMapRenderer mapRenderer;

   // Define world variables
   private World world;
   private Box2DDebugRenderer debugRenderer;
   
   // Define player variables
   private Hero  hero;
   
   public GameScreen(final GameCore gameCore)
   {
      this.gameCore = gameCore;
      gameCam       = new OrthographicCamera();
      viewPort      = new FitViewport(gameCore.V_WIDTH / gameCore.PPM, gameCore.V_HEIGHT / gameCore.PPM, gameCam);
      hud           = new HUD(gameCore.batch);
      mapLoader     = new TmxMapLoader();
      map           = mapLoader.load("levels/level1/Mappening.tmx");
      mapRenderer   = new OrthogonalTiledMapRenderer(map, 1 / GameCore.PPM);
      
      
      gameCam.position.set(viewPort.getWorldWidth() /2, (viewPort.getWorldHeight() /2) -64, 0);
      
      
      world         = new World(new Vector2(0, -10), true);
      debugRenderer = new Box2DDebugRenderer();
      
      hero          = new Hero(world);
      
      // Pull shapes from Objectlayer to handle ground boundaries 
      TileMapObjectParser.parseObjectLayer(world, map.getLayers().get(2).getObjects());
   }


   public void update(float delta)
   {
      controlsHandler(delta);
      
      world.step(1/60f, 6, 2);
      gameCam.position.x = hero.body.getPosition().x;
      gameCam.update();
      mapRenderer.setView(gameCam);
   }
   
   public void controlsHandler(float delta)
   {
      if(Gdx.input.isKeyJustPressed(Input.Keys.UP))
      {
         hero.body.applyLinearImpulse(new Vector2(0,10f), hero.body.getWorldCenter(), true);
      }
      if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) && hero.body.getLinearVelocity().x <= 2)
      {
         hero.body.applyLinearImpulse(new Vector2(1f,0f), hero.body.getWorldCenter(), true);
      }
      if(Gdx.input.isKeyPressed(Input.Keys.LEFT) && hero.body.getLinearVelocity().x >= -2)
      {
         hero.body.applyLinearImpulse(new Vector2(-1f,0f), hero.body.getWorldCenter(), true);
      }
   }
   
   @Override
   public void show()
   {
   }

   @Override
   public void render(float delta)
   {
      update(delta);
      
      Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      // Render world map 
      mapRenderer.render();
      // Render debug lines
      debugRenderer.render(world, gameCam.combined);
      // Render Camera & HUD
      gameCore.batch.setProjectionMatrix(hud.stage.getCamera().combined);
      // Draw HUD
      hud.stage.draw();

   }

   @Override
   public void resize(int width, int height)
   {
      viewPort.update(width,height);

   }

   @Override
   public void pause()
   {
   }

   @Override
   public void resume()
   {
   }

   @Override
   public void hide()
   {
   }

   @Override
   public void dispose()
   {
   }
}

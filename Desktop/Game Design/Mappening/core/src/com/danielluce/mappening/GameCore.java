package com.danielluce.mappening;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import screens.GameScreen;

public class GameCore extends Game
{
   //Core game configuration, referenced by DesktopLauncher.java
   public static final String TITLE    = "CS 3143 Game Design - The Mappening";
   public static final float  VERSION  = 0.1f;
   
   
   public static final int    V_WIDTH  = 640;
   public static final int    V_HEIGHT = 480;
   public static final float  PPM      = 1; //Pixels per Meter
   public static OrthographicCamera  camera;
   public SpriteBatch         batch;
   public AssetManager        assets;

   
   @Override
   public void create()
   {
      batch = new SpriteBatch();
      setScreen(new GameScreen(this));
   }

   @Override
   public void render()
   {
      super.render();
   }

   @Override
   public void dispose()
   {
      batch.dispose();
   }
}

package sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.danielluce.mappening.GameCore;

public class Hero extends Sprite
{
   private GameCore gameCore;
   public  World world;
   public  Body  body;
   
   public Hero(World world)
   {
      this.world = world;
      defineHero();
   }
   
   public void defineHero()
   {
      BodyDef bDef = new BodyDef();
      bDef.position.set(64, 300);
      bDef.type    = BodyDef.BodyType.DynamicBody;
      body         = world.createBody(bDef);
      
      FixtureDef fDef = new FixtureDef();
      CircleShape shape = new CircleShape();
      shape.setRadius(5 / GameCore.PPM);
      fDef.shape = shape;
      body.createFixture(fDef);
   }
}

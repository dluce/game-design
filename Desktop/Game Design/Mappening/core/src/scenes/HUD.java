package scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.danielluce.mappening.GameCore;

public class HUD
{
   public Stage stage;
   private Viewport viewport;
   private Integer score;

   Label scoreLabel;
   Label levelLabel;
   Label worldLabel;
   Label heroLabel;

   public HUD(SpriteBatch sb)
   {
      score    = 0;
      viewport = new FitViewport(GameCore.V_WIDTH, GameCore.V_HEIGHT,
                                 new OrthographicCamera());
      stage = new Stage(viewport, sb);

      //Create layout structure for HUD
      Table table = new Table(); 
      table.bottom();               //Render HUD at bottom of screen
      table.setFillParent(true);    //Fit HUD to parent stage
      
      //Define HUD elements
      scoreLabel = new Label(String.format("%06d", score), new Label.LabelStyle(new BitmapFont(), Color.BLACK));
      levelLabel = new Label("OLD FOREST", new Label.LabelStyle(new BitmapFont(), Color.BLACK));
      worldLabel = new Label("WORLD", new Label.LabelStyle(new BitmapFont(), Color.BLACK));
      heroLabel  = new Label("HERO", new Label.LabelStyle(new BitmapFont(), Color.BLACK));
      
      //Build HUD
      table.add(heroLabel).expandX();
      table.add(worldLabel).expandX();
      table.row();
      table.add(scoreLabel).expandX().padBottom(10);
      table.add(levelLabel).expandX().padBottom(10);
      
      stage.addActor(table);
      
   }
}

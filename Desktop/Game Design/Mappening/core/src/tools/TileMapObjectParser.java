package tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

public class TileMapObjectParser
{
   public static void parseObjectLayer(World world, MapObjects objects)
   {
      for(MapObject object: objects)
      {
         Shape shape;
         if(object instanceof PolylineMapObject)
         {
            shape = createPolyline((PolylineMapObject) object);
         }
         else
         {
            continue;
         }
         Body polyBody;
         BodyDef polyBodyDef = new BodyDef();
         polyBodyDef.type    = BodyDef.BodyType.StaticBody;
         polyBody            = world.createBody(polyBodyDef);
         polyBody.createFixture(shape, 1.0f);
         shape.dispose();
      }
   }
   
   private static ChainShape createPolyline(PolylineMapObject pLine)
   {
      float[]   vertices  = pLine.getPolyline().getTransformedVertices();
      Vector2[] wVertices = new Vector2[vertices.length /2];
      
      for(int i = 0; i < wVertices.length; i++)
      {
         wVertices[i] = new Vector2(vertices[i * 2], vertices[i * 2 + 1]);
      }
      
      ChainShape cs = new ChainShape();
      cs.createChain(wVertices);
      return cs;
   }
}

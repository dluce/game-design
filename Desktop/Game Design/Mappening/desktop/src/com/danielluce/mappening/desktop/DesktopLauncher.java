package com.danielluce.mappening.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.danielluce.mappening.GameCore;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
      config.title         = GameCore.TITLE + " v" + GameCore.VERSION;
      config.width         = GameCore.V_WIDTH;
      config.height        = GameCore.V_HEIGHT;
      config.backgroundFPS = 60;
      config.foregroundFPS = 60;
      config.resizable     = true;
		new LwjglApplication(new GameCore(), config);
	}
}
